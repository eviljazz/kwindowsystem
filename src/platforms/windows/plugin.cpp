/*
    SPDX-FileCopyrightText: 2020 Andre H. Beckedorf <evilJazz@katastrophos.net>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#include "plugin.h"
#include "kwindowinfo_p_win.h"
#include "kwindowsystem_p_win.h"

WindowsPlugin::WindowsPlugin(QObject *parent)
    : KWindowSystemPluginInterface(parent)
{
}

WindowsPlugin::~WindowsPlugin()
{
}

KWindowEffectsPrivate *WindowsPlugin::createEffects()
{
    return nullptr;
}

KWindowSystemPrivate *WindowsPlugin::createWindowSystem()
{
    return new KWindowSystemPrivateWindows();
}

KWindowInfoPrivate *WindowsPlugin::createWindowInfo(WId window, NET::Properties properties, NET::Properties2 properties2)
{
    return new KWindowInfoPrivateWindows(window, properties, properties2);
}

KWindowShadowPrivate *WindowsPlugin::createWindowShadow()
{
    return nullptr;
}

KWindowShadowTilePrivate *WindowsPlugin::createWindowShadowTile()
{
    return nullptr;
}
