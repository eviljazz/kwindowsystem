/*
    SPDX-FileCopyrightText: 2020 Andre H. Beckedorf <evilJazz@katastrophos.net>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/
#ifndef KWINDOWSYSTEM_WIN_PLUGIN_H
#define KWINDOWSYSTEM_WIN_PLUGIN_H

#include "kwindowsystemplugininterface_p.h"

class WindowsPlugin : public KWindowSystemPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.kde.kwindowsystem.KWindowSystemPluginInterface" FILE "windows.json")
    Q_INTERFACES(KWindowSystemPluginInterface)

public:
    explicit WindowsPlugin(QObject *parent = nullptr);
    ~WindowsPlugin() override;

    KWindowEffectsPrivate *createEffects() override;
    KWindowSystemPrivate *createWindowSystem() override;
    KWindowInfoPrivate *createWindowInfo(WId window, NET::Properties properties, NET::Properties2 properties2) override;
    KWindowShadowPrivate *createWindowShadow() override final;
    KWindowShadowTilePrivate *createWindowShadowTile() override final;
};

#endif
