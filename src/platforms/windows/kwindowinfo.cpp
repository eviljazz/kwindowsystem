/*
    This file is part of the KDE libraries
    SPDX-FileCopyrightText: 2008 Carlo Segato <brandon.ml@gmail.com>
    SPDX-FileCopyrightText: 2011 Pau Garcia i Quiles <pgquiles@elpauer.org>
    SPDX-FileCopyrightText: 2020 Andre H. Beckedorf <evilJazz@katastrophos.net>
    
    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include "kwindowinfo.h"
#include "kwindowsystem.h"
#include <windows.h>
#include <stdlib.h>
#include <QCoreApplication>

#include "kwindowinfo_p_win.h"

#include <QRect>

KWindowInfoPrivateWindows::KWindowInfoPrivateWindows(WId _win, NET::Properties properties, NET::Properties2 properties2)
    : KWindowInfoPrivate(_win, properties, properties2),
      KWindowInfoPrivatePidExtension()
{
    installPidExtension(this);
}

KWindowInfoPrivateWindows::~KWindowInfoPrivateWindows()
{
}

bool KWindowInfoPrivateWindows::valid(bool withdrawn_is_valid) const
{
    return true;
}

NET::States KWindowInfoPrivateWindows::state() const
{
    NET::States state = 0;
#ifndef _WIN32_WCE
    if (IsZoomed(reinterpret_cast<HWND>(win()))) {
        state |= NET::Max;
    }
#endif
    if (!IsWindowVisible(reinterpret_cast<HWND>(win()))) {
        state |= NET::Hidden;
    }

#ifndef _WIN32_WCE
    LONG_PTR lp = GetWindowLongPtr(reinterpret_cast<HWND>(win()), GWL_EXSTYLE);
    if (lp & WS_EX_TOOLWINDOW) {
        state |= NET::SkipTaskbar;
    }
#endif

    return state;
}

bool KWindowInfoPrivateWindows::isMinimized() const
{
#ifndef _WIN32_WCE
    return IsIconic(reinterpret_cast<HWND>(win()));
#else
    return false;
#endif
}

NET::MappingState KWindowInfoPrivateWindows::mappingState() const
{
#ifndef _WIN32_WCE
    if (IsIconic(reinterpret_cast<HWND>(win()))) {
        return NET::Iconic;
    }
#endif
    if (!IsWindowVisible(reinterpret_cast<HWND>(win()))) {
        return NET::Withdrawn;
    }
    return NET::Visible;
}

NETExtendedStrut KWindowInfoPrivateWindows::extendedStrut() const
{
    return NETExtendedStrut();
}

NET::WindowType KWindowInfoPrivateWindows::windowType(NET::WindowTypes supported_types) const
{
    NET::WindowType wt(NET::Normal);

    long windowStyle   = GetWindowLong(reinterpret_cast<HWND>(win()), GWL_STYLE);
    long windowStyleEx = GetWindowLong(reinterpret_cast<HWND>(win()), GWL_EXSTYLE);

    if (windowStyle & WS_POPUP && supported_types & NET::PopupMenuMask) {
        return NET::PopupMenu;
    } else if (windowStyleEx & WS_EX_TOOLWINDOW && supported_types & NET::TooltipMask) {
        return NET::Tooltip;
    } else if (!(windowStyle & WS_CHILD) && supported_types & NET::NormalMask) {
        return NET::Normal;
    }

    return wt;
}

QString KWindowInfoPrivateWindows::visibleNameWithState() const
{
    QString s = visibleName();
    if (isMinimized()) {
        s.prepend(QLatin1Char('('));
        s.append(QLatin1Char(')'));
    }
    return s;
}

QString KWindowInfoPrivateWindows::visibleName() const
{
    return name();
}

QString KWindowInfoPrivateWindows::name() const
{
    QByteArray windowText = QByteArray((GetWindowTextLength(reinterpret_cast<HWND>(win())) + 1) * sizeof(wchar_t), 0);
    GetWindowTextW(reinterpret_cast<HWND>(win()), (LPWSTR)windowText.data(), windowText.size());
    return QString::fromWCharArray((wchar_t *)windowText.data());
}

QString KWindowInfoPrivateWindows::visibleIconNameWithState() const
{
    return QString();
}

QString KWindowInfoPrivateWindows::visibleIconName() const
{
    return QString();
}

QString KWindowInfoPrivateWindows::iconName() const
{
    return QString();
}

bool KWindowInfoPrivateWindows::isOnDesktop(int desk) const
{
    return desk == desktop();
}

bool KWindowInfoPrivateWindows::onAllDesktops() const
{
    return false;
}

int KWindowInfoPrivateWindows::desktop() const
{
    return 1;
}

QRect KWindowInfoPrivateWindows::geometry() const
{
    RECT wndRect;
    memset(&wndRect, 0, sizeof(wndRect));

    //fetch the geometry INCLUDING the frames
    if (GetWindowRect(reinterpret_cast<HWND>(win()), &wndRect)) {
        QRect result;
        result.setCoords(wndRect.left, wndRect.top, wndRect.right, wndRect.bottom);
        return result;
    }

    return QRect();
}

QRect KWindowInfoPrivateWindows::frameGeometry() const
{
    RECT wndRect;
    memset(&wndRect, 0, sizeof(wndRect));

    //fetch only client area geometries ... i hope thats right
    if (GetClientRect(reinterpret_cast<HWND>(win()), &wndRect)) {
        QRect result;
        result.setCoords(wndRect.left, wndRect.top, wndRect.right, wndRect.bottom);
        return result;
    }

    return QRect();
}

bool KWindowInfoPrivateWindows::actionSupported(NET::Action action) const
{
    return true; // no idea if it's supported or not -> pretend it is
}

int KWindowInfoPrivateWindows::pid() const
{
    DWORD processId;
    GetWindowThreadProcessId(reinterpret_cast<HWND>(win()), &processId);
    return processId;
}

QByteArray KWindowInfoPrivateWindows::windowClassClass() const
{
//    kWarning(( d->info->passedProperties()[ NETWinInfo::PROTOCOLS2 ] & NET::WM2WindowClass ) == 0, 176 )
//        << "Pass NET::WM2WindowClass to KWindowInfo";
//    return d->info->windowClassClass();

    // Implemented per http://tronche.com/gui/x/icccm/sec-4.html#WM_CLASS (but only 2nd and 3rd choices, -name ignored)
    char *resourcenamevar;
    resourcenamevar = getenv("RESOURCE_NAME");
    if (resourcenamevar != nullptr) {
        return QByteArray(resourcenamevar);
    }

    return QCoreApplication::applicationName().toLocal8Bit();
}

QByteArray KWindowInfoPrivateWindows::windowClassName() const
{
//    kWarning(( d->info->passedProperties()[ NETWinInfo::PROTOCOLS2 ] & NET::WM2WindowClass ) == 0, 176 )
//        << "Pass NET::WM2WindowClass to KWindowInfo";
//    return d->info->windowClassName();

    // Maybe should use RealGetWindowClass instead of GetClassName? See
    // http://blogs.msdn.com/b/oldnewthing/archive/2010/12/31/10110524.aspx

    const int max = 256; // truncate to 255 characters
    TCHAR name[max];
    int count = GetClassName(reinterpret_cast<HWND>(win()), name, max);
    return QString::fromUtf16((ushort *)name).toLocal8Bit();
}

WId KWindowInfoPrivateWindows::transientFor() const
{
    return 0;
}

WId KWindowInfoPrivateWindows::groupLeader() const
{
    return 0;
}

QByteArray KWindowInfoPrivateWindows::windowRole() const
{
    return QByteArray();
}

QByteArray KWindowInfoPrivateWindows::clientMachine() const
{
    return QByteArray();
}

QStringList KWindowInfoPrivateWindows::activities() const
{
    return QStringList();
}
