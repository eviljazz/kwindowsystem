/*
    SPDX-FileCopyrightText: 2020 Andre H. Beckedorf <evilJazz@katastrophos.net>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#ifndef KWINDOWSYSTEM_P_WIN_H
#define KWINDOWSYSTEM_P_WIN_H

#include "kwindowsystem_p.h"

#define EXPERIMENTAL_WINDOW_TRACKING

#include <QObject>
#include <QMap>
#include <QList>
#include <QAbstractNativeEventFilter>
#include <Carbon/Carbon.h>

class KWindowInfo;
struct KWindowInfoPrivateMacWinInfo;
class KWindowSystemPrivateMacEventFilter;

class KWindowSystemPrivateMac : public KWindowSystemPrivate
{
public:
    QList<WId> windows() override;
    QList<WId> stackingOrder() override;
    WId activeWindow() override;
    void activateWindow(WId win, long time) override;
    void forceActiveWindow(WId win, long time) override;
    void demandAttention(WId win, bool set) override;
    bool compositingActive() override;
    int currentDesktop() override;
    int numberOfDesktops() override;
    void setCurrentDesktop(int desktop) override;
    void setOnAllDesktops(WId win, bool b) override;
    void setOnDesktop(WId win, int desktop) override;
    void setOnActivities(WId win, const QStringList &activities) override;
#if KWINDOWSYSTEM_BUILD_DEPRECATED_SINCE(5, 0)
    WId transientFor(WId window) override;
    WId groupLeader(WId window) override;
#endif
    QPixmap icon(WId win, int width, int height, bool scale, int flags) override;
    QPixmap iconFromNetWinInfo(int width, int height, bool scale, int flags, NETWinInfo *info) override;
    void setIcons(WId win, const QPixmap &icon, const QPixmap &miniIcon) override;
    void setType(WId win, NET::WindowType windowType) override;
    void setState(WId win, NET::States state) override;
    void clearState(WId win, NET::States state) override;
    void minimizeWindow(WId win) override;
    void unminimizeWindow(WId win) override;
    void raiseWindow(WId win) override;
    void lowerWindow(WId win) override;
    bool icccmCompliantMappingState() override;
    QRect workArea(int desktop) override;
    QRect workArea(const QList<WId> &excludes, int desktop) override;
    QString desktopName(int desktop) override;
    void setDesktopName(int desktop, const QString &name) override;
    bool showingDesktop() override;
    void setShowingDesktop(bool showing) override;
    void setUserTime(WId win, long time) override;
    void setExtendedStrut(WId win, int left_width, int left_start, int left_end,
                          int right_width, int right_start, int right_end, int top_width, int top_start, int top_end,
                          int bottom_width, int bottom_start, int bottom_end) override;
    void setStrut(WId win, int left, int right, int top, int bottom) override;
    bool allowedActionsSupported() override;
    QString readNameProperty(WId window, unsigned long atom) override;
    void allowExternalProcessWindowActivation(int pid) override;
    void setBlockingCompositing(WId window, bool active) override;
    bool mapViewport() override;
    int viewportToDesktop(const QPoint &pos) override;
    int viewportWindowToDesktop(const QRect &r) override;
    QPoint desktopToViewport(int desktop, bool absolute) override;
    QPoint constrainViewportRelativePosition(const QPoint &pos) override;

    void connectNotify(const QMetaMethod &signal) override;

    enum FilterInfo {
        INFO_BASIC = 1,  // desktop info, not per-window
        INFO_WINDOWS = 2 // also per-window info
    };

private:
    friend class KWindowSystemPrivateMacEventFilter;
    void init(FilterInfo info);
    static KWindowSystemPrivateMacEventFilter *s_d_func();
    static KWindowSystemPrivateMac *self();
};

#include "kwindowinfo_mac_p.h"

class KWindowSystemPrivateMacEventFilter : QObject
{
    Q_OBJECT
public:
    explicit KWindowSystemPrivateMacEventFilter();
    ~KWindowSystemPrivateMacEventFilter() override;

    bool activated;
    void activate();

    QMap<WId, KWindowInfoPrivateMacWinInfo> windows;
    QList<WId> winids; // bah, because KWindowSystem::windows() returns a const reference, we need to keep this separate...
    QMap<pid_t, AXObserverRef> newWindowObservers;
    QMap<pid_t, AXObserverRef> windowClosedObservers;
    QMap<ProcessSerialNumber, WId> processes;

    QList<KWindowInfoPrivateMacWinInfo *> nonProcessedWindows;

    EventTargetRef m_eventTarget;
    EventHandlerUPP m_eventHandler;
    EventTypeSpec m_eventType[4];
    EventHandlerRef m_curHandler;

    void applicationLaunched(const ProcessSerialNumber &psn);
    void applicationTerminated(const ProcessSerialNumber &psn);
    void applicationFrontSwitched(const ProcessSerialNumber &psn);

    bool m_noEmit;
    bool waitingForTimer;

    void newWindow(AXUIElementRef element, void *windowInfoPrivate);
    void windowClosed(AXUIElementRef element, void *windowInfoPrivate);

    static KWindowSystemPrivateMacEventFilter *self()
    {
        return KWindowSystemPrivateMac::s_d_func();
    }
public Q_SLOTS:
    void tryRegisterProcess();
};

#endif
