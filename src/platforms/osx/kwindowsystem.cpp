/*
    This file is part of the KDE libraries
    SPDX-FileCopyrightText: 2007 Laurent Montel <montel@kde.org>
    SPDX-FileCopyrightText: 2008 Marijn Kruisselbrink <m.kruisselbrink@student.tue.nl>
    SPDX-FileCopyrightText: 2020 Andre H. Beckedorf <evilJazz@katastrophos.net>

    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include "kwindowsystem.h"
#include "kwindowsystem_p_mac.h"
#include "kwindowinfo_mac_p.h"

#include <QBitmap>
#include <QDesktopWidget>
#include <QDialog>
#include <QTimer>
#include <QMetaMethod>
#include <QDebug>

#include <Carbon/Carbon.h>

/*! \internal
    Returns the CoreGraphics CGContextRef of the paint device. 0 is
    returned if it can't be obtained. It is the caller's responsibility to
    CGContextRelease the context when finished using it.
    \warning This function is only available on \macos.
    \warning This function is duplicated in qmacstyle_mac.mm as qt_mac_cg_context
*/
CGContextRef kws_qt_mac_cg_context(const QPaintDevice *pdev)
{
    if (pdev->devType() == QInternal::Image) {
         const QImage *i = static_cast<const  QImage*>(pdev);
         QImage *image = const_cast< QImage*>(i);
        CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
        uint flags = kCGImageAlphaPremultipliedFirst;
        flags |= kCGBitmapByteOrder32Host;
        CGContextRef ret = 0;
        ret = CGBitmapContextCreate(image->bits(), image->width(), image->height(),
                                    8, image->bytesPerLine(), colorspace, flags);
        CGContextTranslateCTM(ret, 0, image->height());
        CGContextScaleCTM(ret, 1, -1);
        return ret;
    }
    return 0;
}

// Uncomment the following line to enable the experimental (and not fully functional) window tracking code. Without this
// only the processes/applications are tracked, not the individual windows. This currently is quite broken as I can't
// seem to be able to convince the build system to generate a mov file from both the public header file, and also for this
// private class

static bool operator<(const ProcessSerialNumber &a, const ProcessSerialNumber &b)
{
    if (a.lowLongOfPSN != b.lowLongOfPSN) {
        return a.lowLongOfPSN < b.lowLongOfPSN;
    }
    return a.highLongOfPSN < b.highLongOfPSN;
}

class KWindowSystemStaticContainer2
{
public:
    KWindowSystemStaticContainer2() : d(0) {}
    KWindowSystemPrivateMac kwm;
    KWindowSystemPrivateMacEventFilter *d;
};

Q_GLOBAL_STATIC(KWindowSystemStaticContainer2, g_kwmInstanceContainer2)

static OSStatus applicationEventHandler(EventHandlerCallRef inHandlerCallRef, EventRef inEvent, void *inUserData)
{
    KWindowSystemPrivateMacEventFilter *d = (KWindowSystemPrivateMacEventFilter *) inUserData;

    UInt32 kind;

    kind = GetEventKind(inEvent);
    ProcessSerialNumber psn;
    if (GetEventParameter(inEvent, kEventParamProcessID, typeProcessSerialNumber, nullptr, sizeof psn, nullptr, &psn) != noErr) {
        qWarning() << "Error getting event parameter in application event";
        return eventNotHandledErr;
    }

    switch (kind)
    {
    case kEventAppLaunched:
        d->applicationLaunched(psn);
        break;
    case kEventAppTerminated:
        d->applicationTerminated(psn);
        break;
    case kEventAppFrontSwitched:
        d->applicationFrontSwitched(psn);
        break;
    default:
        break;
    }

    return noErr;
}

static void windowClosedObserver(AXObserverRef observer, AXUIElementRef element, CFStringRef notification, void *refcon)
{
    KWindowSystemPrivateMacEventFilter::self()->windowClosed(element, refcon);
}

static void newWindowObserver(AXObserverRef observer, AXUIElementRef element, CFStringRef notification, void *refcon)
{
    KWindowSystemPrivateMacEventFilter::self()->newWindow(element, refcon);
}


/* KWindowSystemPrivateMacEventFilter */

KWindowSystemPrivateMacEventFilter::KWindowSystemPrivateMacEventFilter()
    : QObject(0), activated(false), m_noEmit(true), waitingForTimer(false)
{
    // find all existing windows
    ProcessSerialNumber psn = {0, kNoProcess};
    while (GetNextProcess(&psn) == noErr) {
        qDebug() << "calling appLaunched for " << psn.lowLongOfPSN << ":" << psn.highLongOfPSN;
        applicationLaunched(psn);
    }

    m_noEmit = false;

    // register callbacks for application launches/quits
    m_eventTarget = GetApplicationEventTarget();
    m_eventHandler = NewEventHandlerUPP(applicationEventHandler);
    m_eventType[0].eventClass = kEventClassApplication;
    m_eventType[0].eventKind = kEventAppLaunched;
    m_eventType[1].eventClass = kEventClassApplication;
    m_eventType[1].eventKind = kEventAppTerminated;
    m_eventType[2].eventClass = kEventClassApplication;
    m_eventType[2].eventKind = kEventAppFrontSwitched;
    if (InstallEventHandler(m_eventTarget, m_eventHandler, 3, m_eventType, this, &m_curHandler) != noErr) {
        qDebug() << "Installing event handler failed!\n";
    }
}

KWindowSystemPrivateMacEventFilter::~KWindowSystemPrivateMacEventFilter()
{

}

void KWindowSystemPrivateMacEventFilter::applicationLaunched(const ProcessSerialNumber &psn)
{
    qDebug() << "new app: " << psn.lowLongOfPSN << ":" << psn.highLongOfPSN;
    ProcessInfoRec pinfo;

    pinfo.processInfoLength = sizeof pinfo;
    pinfo.processName = nullptr;

#ifdef Q_OS_MAC32
    FSSpec appSpec;
    pinfo.processAppSpec = &appSpec;
#else
    FSRef appRef;
    pinfo.processAppRef = &appRef;
#endif
    GetProcessInformation(&psn, &pinfo);
    if ((pinfo.processMode & modeOnlyBackground) != 0) {
        return;
    }

    // found a process, create a pseudo-window for it

    WId wid = psn.highLongOfPSN << 32 | psn.lowLongOfPSN;

    KWindowInfoPrivateMacWinInfo *winInfo = &windows[wid];
    winids.append(wid);

    winInfo->setProcessSerialNumber(psn);
    pid_t pid = winInfo->pid();
    processes[psn] = wid;
    qDebug() << "  pid:" << pid;

    AXUIElementRef app = AXUIElementCreateApplication(pid);
    winInfo->setAxElement(app);

    if (!m_noEmit) {
        emit KWindowSystem::self()->windowAdded(wid);
    }

    // create an observer and listen for new window events
    AXObserverRef observer, newObserver;
    OSStatus err;

    if (AXObserverCreate(pid, windowClosedObserver, &observer) == noErr) {
        CFRunLoopAddSource(CFRunLoopGetCurrent(), AXObserverGetRunLoopSource(observer), kCFRunLoopCommonModes);
        windowClosedObservers[pid] = observer;
    }

    if ((err = AXObserverCreate(pid, newWindowObserver, &newObserver)) == noErr) {
        CFRunLoopAddSource(CFRunLoopGetCurrent(), AXObserverGetRunLoopSource(newObserver), kCFRunLoopCommonModes);
        newWindowObservers[pid] = newObserver;

        if ((err = AXObserverAddNotification(newObserver, app, kAXWindowCreatedNotification, winInfo)) != noErr) {
            qDebug() << "Error " << err << " adding notification to observer";
            // adding notifier failed, apparently app isn't responding to accesability messages yet
            // try it one more time later, and for now just return
            QTimer::singleShot(500, this, SLOT(tryRegisterProcess()));
            nonProcessedWindows.append(winInfo);
            return;
        } else {
            qDebug() << "Added notification and observer";
        }
    } else {
        qDebug() << "Error creating observer";
    }

    CFIndex windowsInApp;
    AXUIElementGetAttributeValueCount(app, kAXWindowsAttribute, &windowsInApp);
    CFArrayRef array;
    AXUIElementCopyAttributeValue(app, kAXWindowsAttribute, (CFTypeRef *)&array);
    for (CFIndex j = 0; j < windowsInApp; j++) {
        AXUIElementRef win = (AXUIElementRef) CFArrayGetValueAtIndex(array, j);
        newWindow(win, winInfo);
    }
}

void KWindowSystemPrivateMacEventFilter::tryRegisterProcess()
{
    qDebug() << "Single-shot timer, trying to register processes";
    while (!nonProcessedWindows.empty()) {
        KWindowInfoPrivateMacWinInfo *winInfo = nonProcessedWindows.takeLast();

        pid_t pid = winInfo->pid();
        AXUIElementRef app = winInfo->axElement();
        ProcessSerialNumber psn = winInfo->psn();

        // create an observer and listen for new window events
        AXObserverRef observer;
        OSStatus err;
        observer = newWindowObservers[pid];
        if ((err = AXObserverAddNotification(observer, app, kAXWindowCreatedNotification, winInfo)) != noErr) {
            qDebug() << "Error " << err << " adding notification to observer";
        } else {
            qDebug() << "Added notification and observer";
        }

        observer = windowClosedObservers[pid];

        CFIndex windowsInApp;
        AXUIElementGetAttributeValueCount(app, kAXWindowsAttribute, &windowsInApp);
        CFArrayRef array;
        AXUIElementCopyAttributeValue(app, kAXWindowsAttribute, (CFTypeRef *)&array);
        for (CFIndex j = 0; j < windowsInApp; j++) {
            AXUIElementRef win = (AXUIElementRef) CFArrayGetValueAtIndex(array, j);
            newWindow(win, winInfo);
        }
    }
}

void KWindowSystemPrivateMacEventFilter::applicationTerminated(const ProcessSerialNumber &psn)
{
    qDebug() << "Terminated PSN: " << psn.lowLongOfPSN << ":" << psn.highLongOfPSN;
    WId id = processes[psn];

    if (windows.contains(id)) {
        KWindowInfoPrivateMacWinInfo *winInfo = &windows[id];

        for (KWindowInfoPrivateMacWinInfo *wi : winInfo->children) {
            winids.removeAll(wi->win);
            emit KWindowSystem::self()->windowRemoved(wi->win);
        }

        winids.removeAll(id);
        emit KWindowSystem::self()->windowRemoved(id);
    }
}

void KWindowSystemPrivateMacEventFilter::applicationFrontSwitched(const ProcessSerialNumber &psn)
{
    qDebug() << "Activated PSN: " << psn.lowLongOfPSN << ":" << psn.highLongOfPSN;
    WId id = processes[psn];

    if (windows.contains(id)) {
        emit KWindowSystem::self()->activeWindowChanged(id);
    }
}

void KWindowSystemPrivateMacEventFilter::windowClosed(AXUIElementRef element, void *windowInfoPrivate)
{
    qDebug() << "Received window closed notification";

    KWindowInfoPrivateMacWinInfo *wind = (KWindowInfoPrivateMacWinInfo *) windowInfoPrivate; // window being closed
    KWindowInfoPrivateMacWinInfo *parent = wind->parent;
    parent->children.removeAll(wind);
    winids.removeAll(wind->win);
    if (!m_noEmit) {
        emit KWindowSystem::self()->windowRemoved(wind->win);
    }
}

void KWindowSystemPrivateMacEventFilter::newWindow(AXUIElementRef win, void *windowInfoPrivate)
{
    qDebug() << "Received new window notification";

    KWindowInfoPrivateMacWinInfo *parentWinInfo = (KWindowInfoPrivateMacWinInfo *) windowInfoPrivate;
    pid_t pid = parentWinInfo->pid();
    ProcessSerialNumber psn = parentWinInfo->psn();
    AXObserverRef observer = windowClosedObservers[pid];

    WId wid = reinterpret_cast<WId>(win);
    KWindowInfoPrivateMacWinInfo *winInfo = &windows[wid];

    // listen for closed events for this window
    if (AXObserverAddNotification(observer, win, kAXUIElementDestroyedNotification, winInfo) != noErr) {
        // when we can't receive close events, the window should not be added
        qDebug() << "error adding closed observer to window.";
        return;
    }

    //windows[winInfo->win] = *winInfo;
    winids.append(wid);
    winInfo->setProcessSerialNumber(psn);
    winInfo->setAxElement(win);
    parentWinInfo->children.append(winInfo);
    winInfo->parent = parentWinInfo;
    if (!m_noEmit) {
        emit KWindowSystem::self()->windowAdded(wid);
    }
}

void KWindowSystemPrivateMacEventFilter::activate()
{
    //prevent us from doing the same over and over again
    if (activated) {
        return;
    }
    activated = true;
    //fetch window infos
    //reloadStackList();
}

/* KWindowSystemPrivateMac */

KWindowSystemPrivateMac *KWindowSystemPrivateMac::self()
{
    return &(g_kwmInstanceContainer2()->kwm);
}

KWindowSystemPrivateMacEventFilter *KWindowSystemPrivateMac::s_d_func()
{
    KWindowSystemStaticContainer2 *g = g_kwmInstanceContainer2();
    return g->d;
}

void KWindowSystemPrivateMac::init(FilterInfo what)
{
    KWindowSystemPrivateMacEventFilter *s_d = s_d_func();

    if (!s_d) {
        g_kwmInstanceContainer2()->d = new KWindowSystemPrivateMacEventFilter();
        g_kwmInstanceContainer2()->d->activate();
    }
}

QList<WId> KWindowSystemPrivateMac::windows()
{
    KWindowSystemPrivateMacEventFilter *d = KWindowSystemPrivateMac::s_d_func();
    return d->winids;
}

QList<WId> KWindowSystemPrivateMac::stackingOrder()
{
    KWindowSystemPrivateMacEventFilter *d = KWindowSystemPrivateMac::s_d_func();
    return d->winids;
}

WId KWindowSystemPrivateMac::activeWindow()
{
    //return something
    qDebug() << "WId KWindowSystem::activeWindow()   isn't yet implemented!";
    return 0;
}

void KWindowSystemPrivateMac::activateWindow(WId win, long time)
{
    //TODO
    qDebug() << "KWindowSystem::activateWindow( WId win, long time )isn't yet implemented!";
    KWindowSystemPrivateMacEventFilter *ef = KWindowSystemPrivateMac::s_d_func();

    if (ef->windows.contains(win)) {
        KWindowInfoPrivateMacWinInfo *winInfo = &ef->windows[win];
        ProcessSerialNumber psn = winInfo->psn();
        SetFrontProcess(&psn);
    }
}

void KWindowSystemPrivateMac::forceActiveWindow(WId win, long time)
{
    //TODO
    qDebug() << "KWindowSystem::forceActiveWindow( WId win, long time ) isn't yet implemented!";
    activateWindow(win, time);
}

void KWindowSystemPrivateMac::demandAttention(WId win, bool set)
{
    //TODO
    qDebug() << "KWindowSystem::demandAttention( WId win, bool set ) isn't yet implemented!";
}

bool KWindowSystemPrivateMac::compositingActive()
{
    return true;
}

int KWindowSystemPrivateMac::currentDesktop()
{
    return 1;
}

int KWindowSystemPrivateMac::numberOfDesktops()
{
    return 1;
}

void KWindowSystemPrivateMac::setCurrentDesktop(int desktop)
{
    qDebug() << "KWindowSystem::setCurrentDesktop( int desktop ) isn't yet implemented!";
    //TODO
}

void KWindowSystemPrivateMac::setOnAllDesktops(WId win, bool b)
{
    qDebug() << "KWindowSystem::setOnAllDesktops( WId win, bool b ) isn't yet implemented!";
    //TODO
}

void KWindowSystemPrivateMac::setOnDesktop(WId win, int desktop)
{
    //TODO
    qDebug() << "KWindowSystem::setOnDesktop( WId win, int desktop ) isn't yet implemented!";
}

QPixmap KWindowSystemPrivateMac::icon(WId win, int width, int height, bool scale, int)
{
    if (KWindowSystem::self()->hasWId(win)) {
        KWindowInfo info(win, 0);
        KWindowSystemPrivateMacEventFilter *ef = KWindowSystemPrivateMac::s_d_func();
        KWindowInfoPrivateMacWinInfo *winInfo = &ef->windows[win];

        if (!winInfo->loadedData) {
            winInfo->updateData();
        }
        IconRef icon;
        SInt16 label;
#ifdef Q_OS_MAC32
        OSErr err = GetIconRefFromFile(&winInfo->iconSpec, &icon, &label);
#else
        OSStatus err = GetIconRefFromFileInfo(&winInfo->iconSpec, 0, 0,
                                              kIconServicesCatalogInfoMask, 0, kIconServicesNormalUsageFlag, &icon, &label);
#endif
        if (err != noErr) {
            qDebug() << "Error getting icon from application";
            return QPixmap();
        } else {
            QPixmap ret(width, height);
            ret.fill(QColor(0, 0, 0, 0));

            CGRect rect = CGRectMake(0, 0, width, height);

            CGContextRef ctx = kws_qt_mac_cg_context(&ret);
            CGAffineTransform old_xform = CGContextGetCTM(ctx);
            CGContextConcatCTM(ctx, CGAffineTransformInvert(old_xform));
            CGContextConcatCTM(ctx, CGAffineTransformIdentity);

            ::RGBColor b;
            b.blue = b.green = b.red = 255 * 255;
            PlotIconRefInContext(ctx, &rect, kAlignNone, kTransformNone, &b, kPlotIconRefNormalFlags, icon);
            CGContextRelease(ctx);

            ReleaseIconRef(icon);
            return ret;
        }
    } else {
        qDebug() << "QPixmap KWindowSystem::icon( WId win, int width, int height, bool scale ) isn't yet implemented for local windows!";
        return QPixmap();
    }
}

QPixmap KWindowSystemPrivateMac::iconFromNetWinInfo(int, int, bool, int, NETWinInfo *)
{
    qDebug() << "KWindowSystem::iconFromNetWinInfo is not supported on macOS!";
    return QPixmap();
}

void KWindowSystemPrivateMac::setIcons(WId win, const QPixmap &icon, const QPixmap &miniIcon)
{
    //TODO
    qDebug() << "KWindowSystem::setIcons( WId win, const QPixmap& icon, const QPixmap& miniIcon ) isn't yet implemented!";
}

void KWindowSystemPrivateMac::setType(WId winid, NET::WindowType windowType)
{
#ifdef Q_OS_MAC32
    // not supported for 'global' windows; only for windows in the current process
    if (hasWId(winid)) {
        return;
    }

    static WindowGroupRef desktopGroup = 0;
    static WindowGroupRef dockGroup = 0;

    WindowRef win = HIViewGetWindow((HIViewRef) winid);
    //TODO: implement other types than Desktop and Dock
    if (windowType != NET::Desktop && windowType != NET::Dock) {
        qDebug() << "setType( WId win, NET::WindowType windowType ) isn't yet implemented for the type you requested!";
    }
    if (windowType == NET::Desktop) {
        if (!desktopGroup) {
            CreateWindowGroup(0, &desktopGroup);
            SetWindowGroupLevel(desktopGroup, kCGDesktopIconWindowLevel);
        }
        SetWindowGroup(win, desktopGroup);
    } else if (windowType == NET::Dock) {
        if (!dockGroup) {
            CreateWindowGroup(0, &dockGroup);
            SetWindowGroupLevel(dockGroup, kCGDockWindowLevel);
        }
        SetWindowGroup(win, dockGroup);
        ChangeWindowAttributes(win, kWindowNoTitleBarAttribute, kWindowNoAttributes);
    }
#else
#warning port me to Mac64
#endif
}

void KWindowSystemPrivateMac::setState(WId win, NET::States state)
{
    //TODO
    qDebug() << "KWindowSystem::setState( WId win, unsigned long state ) isn't yet implemented!";
}

void KWindowSystemPrivateMac::clearState(WId win, NET::States state)
{
    //TODO
    qDebug() << "KWindowSystem::clearState( WId win, unsigned long state ) isn't yet implemented!";
}

void KWindowSystemPrivateMac::minimizeWindow(WId win)
{
    //TODO
    qDebug() << "KWindowSystem::minimizeWindow( WId win) isn't yet implemented!";
}

void KWindowSystemPrivateMac::unminimizeWindow(WId win)
{
    //TODO
    qDebug() << "KWindowSystem::unminimizeWindow( WId win ) isn't yet implemented!";
}

void KWindowSystemPrivateMac::raiseWindow(WId win)
{
    //TODO
    qDebug() << "KWindowSystem::raiseWindow( WId win ) isn't yet implemented!";
}

void KWindowSystemPrivateMac::lowerWindow(WId win)
{
    //TODO
    qDebug() << "KWindowSystem::lowerWindow( WId win ) isn't yet implemented!";
}

bool KWindowSystemPrivateMac::icccmCompliantMappingState()
{
    return false;
}

QRect KWindowSystemPrivateMac::workArea(int desktop)
{
    //TODO
    qDebug() << "QRect KWindowSystem::workArea( int desktop ) isn't yet implemented!";
    return QRect();
}

QRect KWindowSystemPrivateMac::workArea(const QList<WId> &exclude, int desktop)
{
    //TODO
    qDebug() << "QRect KWindowSystem::workArea( const QList<WId>& exclude, int desktop ) isn't yet implemented!";
    return QRect();
}

QString KWindowSystemPrivateMac::desktopName(int desktop)
{
    return QObject::tr("Desktop %1").arg(desktop);
}

void KWindowSystemPrivateMac::setDesktopName(int desktop, const QString &name)
{
    qDebug() << "KWindowSystem::setDesktopName( int desktop, const QString& name ) isn't yet implemented!";
    //TODO
}

bool KWindowSystemPrivateMac::showingDesktop()
{
    return false;
}

void KWindowSystemPrivateMac::setUserTime(WId win, long time)
{
    qDebug() << "KWindowSystem::setUserTime( WId win, long time ) isn't yet implemented!";
    //TODO
}

void KWindowSystemPrivateMac::setExtendedStrut(WId win, int left_width, int left_start, int left_end,
                                     int right_width, int right_start, int right_end, int top_width, int top_start, int top_end,
                                     int bottom_width, int bottom_start, int bottom_end)
{
    qDebug() << "KWindowSystem::setExtendedStrut isn't yet implemented!";
    //TODO
}

void KWindowSystemPrivateMac::setStrut(WId win, int left, int right, int top, int bottom)
{
    qDebug() << "KWindowSystem::setStrut isn't yet implemented!";
    //TODO
}

bool KWindowSystemPrivateMac::allowedActionsSupported()
{
    return false;
}

QString KWindowSystemPrivateMac::readNameProperty(WId window, unsigned long atom)
{
    //TODO
    qDebug() << "QString KWindowSystem::readNameProperty( WId window, unsigned long atom ) isn't yet implemented!";
    return QString();
}

void KWindowSystemPrivateMac::connectNotify(const QMetaMethod &method)
{
    FilterInfo what = INFO_BASIC;
    if (method == QMetaMethod::fromSignal(&KWindowSystem::workAreaChanged)) {
        what = INFO_WINDOWS;
    } else if (method == QMetaMethod::fromSignal(&KWindowSystem::strutChanged)) {
        what = INFO_WINDOWS;
    } else if (method == QMetaMethod::fromSignal(static_cast<void(KWindowSystem::*)(WId, const ulong *)>(&KWindowSystem::windowChanged))) {
        what = INFO_WINDOWS;
    } else if (method == QMetaMethod::fromSignal(static_cast<void(KWindowSystem::*)(WId, uint)>(&KWindowSystem::windowChanged))) {
        what = INFO_WINDOWS;
    } else if (method == QMetaMethod::fromSignal(static_cast<void(KWindowSystem::*)(WId)>(&KWindowSystem::windowChanged))) {
        what = INFO_WINDOWS;
    }

    init(what);
}

void KWindowSystemPrivateMac::allowExternalProcessWindowActivation(int pid)
{
    // Needed on mac ?
}

void KWindowSystemPrivateMac::setBlockingCompositing(WId window, bool active)
{
    //TODO
    qDebug() << "setBlockingCompositing( WId window, bool active ) isn't yet implemented!";
}


#if KWINDOWSYSTEM_BUILD_DEPRECATED_SINCE(5, 0)
WId KWindowSystemPrivateMac::transientFor(WId window)
{
    Q_UNUSED(window)
    return 0;
}

WId KWindowSystemPrivateMac::groupLeader(WId window)
{
    Q_UNUSED(window)
    return 0;
}
#endif

void KWindowSystemPrivateMac::setShowingDesktop(bool showing)
{
    Q_UNUSED(showing);
}

void KWindowSystemPrivateMac::setOnActivities(WId win, const QStringList &activities)
{
    Q_UNUSED(win)
    Q_UNUSED(activities)
}

bool KWindowSystemPrivateMac::mapViewport()
{
    return false;
}

int KWindowSystemPrivateMac::viewportToDesktop(const QPoint &pos)
{
    Q_UNUSED(pos)
    return 0;
}

int KWindowSystemPrivateMac::viewportWindowToDesktop(const QRect &r)
{
    Q_UNUSED(r)
    return 0;
}

QPoint KWindowSystemPrivateMac::desktopToViewport(int desktop, bool absolute)
{
    Q_UNUSED(desktop)
    Q_UNUSED(absolute)
    return QPoint();
}

QPoint KWindowSystemPrivateMac::constrainViewportRelativePosition(const QPoint &pos)
{
    Q_UNUSED(pos)
    return QPoint();
}

#include "moc_kwindowsystem.cpp"
