/*
    This file is part of the KDE libraries
    SPDX-FileCopyrightText: 2008 Marijn Kruisselbrink <m.kruisselbrink@student.tue.nl>
    SPDX-FileCopyrightText: 2020 Andre H. Beckedorf <evilJazz@katastrophos.net>

    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include "kwindowinfo_mac_p.h"
#include "kwindowinfo_p_mac.h"
#include "kwindowinfo.h"
#include "kwindowsystem.h"
#include "kwindowsystem_p_mac.h"

#include <QDebug>
#include <netwm_def.h>
#include <QBitmap>
#include <QDesktopWidget>
#include <QDialog>

/* KWindowInfoPrivateMacInternal */

KWindowInfoPrivateMacWinInfo::KWindowInfoPrivateMacWinInfo()
    : ref(0), win(0), isLocal(false), loadedData(false), m_axWin(0), parent(), m_pid(-1)
{
}

void KWindowInfoPrivateMacWinInfo::setAxElement(const AXUIElementRef &axWin)
{
    m_axWin = axWin;
    CFRetain(m_axWin);
}

void KWindowInfoPrivateMacWinInfo::setProcessSerialNumber(const ProcessSerialNumber &psn)
{
    m_psn = psn;
    GetProcessPID(&psn, &m_pid);
}

KWindowInfoPrivateMacWinInfo::~KWindowInfoPrivateMacWinInfo()
{
    if (m_axWin) {
        CFRelease(m_axWin);
    }
}

void KWindowInfoPrivateMacWinInfo::updateData()
{
    ProcessInfoRec pinfo;
    char processName[512];
#ifdef Q_OS_MAC32
    FSSpec appSpec;
#else
    FSRef ref;
#endif
    pinfo.processInfoLength = sizeof pinfo;
    pinfo.processName = (unsigned char *) processName;
#ifdef Q_OS_MAC32
    pinfo.processAppSpec = &appSpec;
#else
    pinfo.processAppRef = &ref;
#endif
    GetProcessInformation(&m_psn, &pinfo);
    name = QString::fromLatin1(processName + 1, processName[0]);

    if (m_axWin) {
        CFStringRef title;
        if (AXUIElementCopyAttributeValue(m_axWin, kAXTitleAttribute, (CFTypeRef *)&title) == noErr) {
            CFStringGetCString(title, processName, sizeof processName, kCFStringEncodingUTF8);
            name = QString::fromUtf8(processName);
        }
    }

#ifdef Q_OS_MAC32
    iconSpec = appSpec;

    FSRef ref;
    FSpMakeFSRef(&appSpec, &ref);
#else
    iconSpec = ref;
#endif
    // check if it is in an application bundle (foo.app/Contents/MacOS/plasma)
    HFSUniStr255 name;
    FSRef parentRef;
    FSGetCatalogInfo(&ref, kFSCatInfoNone, 0, &name, 0, &parentRef);
    ref = parentRef;
    FSGetCatalogInfo(&ref, kFSCatInfoNone, 0, &name, 0, &parentRef);
    if (QString::fromUtf16(name.unicode, name.length) == QStringLiteral("MacOS")) {
        ref = parentRef;
        FSGetCatalogInfo(&ref, kFSCatInfoNone, 0, &name, 0, &parentRef);
        if (QString::fromUtf16(name.unicode, name.length) == QStringLiteral("Contents")) {
#ifdef Q_OS_MAC32
            FSSpec spec;
            ref = parentRef;
            FSGetCatalogInfo(&ref, kFSCatInfoNone, 0, &name, &spec, &parentRef);
            iconSpec = spec;
#else
            iconSpec = parentRef;
#endif
        }
    }

    loadedData = true;
}


/* KWindowInfoPrivateMac */

KWindowInfoPrivateMac::KWindowInfoPrivateMac(WId window, NET::Properties properties, NET::Properties2 properties2)
    : KWindowInfoPrivate(window, properties, properties2),
      KWindowInfoPrivatePidExtension(),
      m_info(nullptr)
{
    installPidExtension(this);

    KWindowSystemPrivateMacEventFilter *ef = KWindowSystemPrivateMacEventFilter::self();

    if (ef->windows.contains(window)) {
        m_info = &ef->windows[window];
    }
}

KWindowInfoPrivateMac::~KWindowInfoPrivateMac()
{
}

bool KWindowInfoPrivateMac::valid(bool withdrawn_is_valid) const
{
    return m_info && m_info->pid() >= 0;
}

NET::States KWindowInfoPrivateMac::state() const
{
    return 0;
}

bool KWindowInfoPrivateMac::isMinimized() const
{
    if (m_info && m_info->axElement()) {
        CFBooleanRef val;
        if (AXUIElementCopyAttributeValue(m_info->axElement(), kAXMinimizedAttribute, (CFTypeRef *)&val) == noErr) {
            return CFBooleanGetValue(val);
        } else {
            return false;
        }
    } else {
        return false;
    }
}

NET::MappingState KWindowInfoPrivateMac::mappingState() const
{
    return (NET::MappingState) 0;
}

NETExtendedStrut KWindowInfoPrivateMac::extendedStrut() const
{
    NETExtendedStrut ext;
    return ext;
}

NET::WindowType KWindowInfoPrivateMac::windowType(NET::WindowTypes supported_types) const
{
    return (NET::WindowType) 0;
}

QString KWindowInfoPrivateMac::visibleNameWithState() const
{
    QString s = visibleName();
    if (isMinimized()) {
        s.prepend(QLatin1Char('('));
        s.append(QLatin1Char(')'));
    }
    return s;
}

QString KWindowInfoPrivateMac::visibleName() const
{
    return name();
}

QString KWindowInfoPrivateMac::name() const
{
    if (!m_info) {
        return QString();
    }

    if (!m_info->loadedData) {
        m_info->updateData();
    }
    return m_info->name;
}

QString KWindowInfoPrivateMac::visibleIconNameWithState() const
{
    QString s = visibleIconName();
    if (isMinimized()) {
        s.prepend(QLatin1Char('('));
        s.append(QLatin1Char(')'));
    }
    return s;
}

QString KWindowInfoPrivateMac::visibleIconName() const
{
    return visibleName();
}

QString KWindowInfoPrivateMac::iconName() const
{
    return name();
}

bool KWindowInfoPrivateMac::isOnDesktop(int _desktop) const
{
    return true;
}

bool KWindowInfoPrivateMac::onAllDesktops() const
{
    return false;
}

int KWindowInfoPrivateMac::desktop() const
{
    return 0;
}

QRect KWindowInfoPrivateMac::geometry() const
{
    return QRect();
}

QRect KWindowInfoPrivateMac::frameGeometry() const
{
    return QRect();
}

bool KWindowInfoPrivateMac::actionSupported(NET::Action action) const
{
    return true; // no idea if it's supported or not -> pretend it is
}

int KWindowInfoPrivateMac::pid() const
{
    return m_info ? m_info->pid() : -1;
}

QByteArray KWindowInfoPrivateMac::windowClassClass() const
{
    return name().toUtf8();
}

QByteArray KWindowInfoPrivateMac::windowClassName() const
{
    return name().toUtf8();
}

WId KWindowInfoPrivateMac::transientFor() const
{
    return 0;
}

WId KWindowInfoPrivateMac::groupLeader() const
{
    return 0;
}

QByteArray KWindowInfoPrivateMac::windowRole() const
{
    return QByteArray();
}

QByteArray KWindowInfoPrivateMac::clientMachine() const
{
    return QByteArray();
}

QStringList KWindowInfoPrivateMac::activities() const
{
    return QStringList();
}
