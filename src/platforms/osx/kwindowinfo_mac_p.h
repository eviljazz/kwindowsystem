/*
    This file is part of the KDE libraries
    SPDX-FileCopyrightText: 2008 Marijn Kruisselbrink <m.kruisselbrink@student.tue.nl>
    SPDX-FileCopyrightText: 2020 Andre H. Beckedorf <evilJazz@katastrophos.net>

    SPDX-License-Identifier: LGPL-2.1-or-later
*/
#ifndef KWINDOWINFO_MAC_P_H
#define KWINDOWINFO_MAC_P_H

#include "kwindowinfo.h"
#include <Carbon/Carbon.h>
#include <QString>
#include <QList>

// bah, why do header files invade my namespace and define such normal words as check...
#ifdef check
#undef check
#endif

struct KWindowInfoPrivateMacWinInfo {
    KWindowInfoPrivateMacWinInfo();
    ~KWindowInfoPrivateMacWinInfo();
    int ref;
    WId win;
    bool isLocal;

    AXUIElementRef axElement() const
    {
        return m_axWin;
    }
    void setAxElement(const AXUIElementRef &axWin);

    ProcessSerialNumber psn() const
    {
        return m_psn;
    }

    void setProcessSerialNumber(const ProcessSerialNumber &psn);

    pid_t pid() const
    {
        return m_pid;
    }

    QString name;
#ifdef Q_OS_MAC32
    FSSpec iconSpec;
#else
    FSRef iconSpec;
#endif

    bool loadedData;
    void updateData();

    AXUIElementRef m_axWin;
    QList<KWindowInfoPrivateMacWinInfo *> children;

    KWindowInfoPrivateMacWinInfo *parent;
private:
    ProcessSerialNumber m_psn;
    pid_t m_pid;
};

#endif

