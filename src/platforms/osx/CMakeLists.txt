set(osx_plugin_SRCS
    kkeyserver.cpp
    kwindowinfo.cpp
    kwindowinfo_p_mac.h
    kwindowinfo_mac_p.h
    kwindowsystem.cpp
    kwindowsystem_p_mac.h
    plugin.cpp
)

add_library(KF5WindowSystemMacPlugin MODULE ${osx_plugin_SRCS})
target_link_libraries(KF5WindowSystemMacPlugin
    KF5WindowSystem
    "-framework CoreFoundation -framework Carbon"
)

set_target_properties(
    KF5WindowSystemMacPlugin
    PROPERTIES LIBRARY_OUTPUT_DIRECTORY "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/kf5/org.kde.kwindowsystem.platforms"
)

install(
    TARGETS
        KF5WindowSystemMacPlugin
    DESTINATION
        ${PLUGIN_INSTALL_DIR}/kf5/org.kde.kwindowsystem.platforms/
)
