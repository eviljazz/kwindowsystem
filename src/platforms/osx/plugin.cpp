/*
    SPDX-FileCopyrightText: 2020 Andre H. Beckedorf <evilJazz@katastrophos.net>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#include "plugin.h"
#include "kwindowinfo_p_mac.h"
#include "kwindowsystem_p_mac.h"

MacPlugin::MacPlugin(QObject *parent)
    : KWindowSystemPluginInterface(parent)
{
}

MacPlugin::~MacPlugin()
{
}

KWindowEffectsPrivate *MacPlugin::createEffects()
{
    return nullptr;
}

KWindowSystemPrivate *MacPlugin::createWindowSystem()
{
    return new KWindowSystemPrivateMac();
}

KWindowInfoPrivate *MacPlugin::createWindowInfo(WId window, NET::Properties properties, NET::Properties2 properties2)
{
    return new KWindowInfoPrivateMac(window, properties, properties2);
}

KWindowShadowPrivate *MacPlugin::createWindowShadow()
{
    return nullptr;
}

KWindowShadowTilePrivate *MacPlugin::createWindowShadowTile()
{
    return nullptr;
}
