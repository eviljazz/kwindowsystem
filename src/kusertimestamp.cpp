/*
    This file is part of the KDE libraries
    SPDX-FileCopyrightText: 2003 Luboš Luňák <l.lunak@kde.org>
    SPDX-FileCopyrightText: 2020 Andre H. Beckedorf <evilJazz@katastrophos.net>

    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include "kusertimestamp.h"

#include "config-kwindowsystem.h"
#include "kwindowsystem.h"
#include <QGuiApplication>

#if KWINDOWSYSTEM_HAVE_X11
#include <netwm.h>
#include <QX11Info>
#elif defined Q_OS_WIN
#include <windows.h>
#elif defined Q_OS_MAC
#include <Carbon/Carbon.h>
#endif

unsigned long KUserTimestamp::userTimestamp()
{
#if KWINDOWSYSTEM_HAVE_X11
    if (KWindowSystem::isPlatformX11()) {
        return QX11Info::appUserTime();
    }
#elif defined Q_OS_WIN
    unsigned long idle = 0;
    LASTINPUTINFO info;
    info.cbSize = sizeof(LASTINPUTINFO);
    if (::GetLastInputInfo(&info)) {
        idle = ::GetTickCount() - info.dwTime;
    }
    return idle;
#elif defined Q_OS_MAC
    return (unsigned long)1000 * ::CGEventSourceSecondsSinceLastEventType(kCGEventSourceStateCombinedSessionState, kCGAnyInputEventType);
#endif
    return 0;
}

void KUserTimestamp::updateUserTimestamp(unsigned long time)
{
#if KWINDOWSYSTEM_HAVE_X11
    if (!KWindowSystem::isPlatformX11()) {
        return;
    }
    if (time == 0) { // get current X timestamp
        time = QX11Info::getTimestamp();
    }

    if (QX11Info::appUserTime() == 0
            || NET::timestampCompare(time, QX11Info::appUserTime()) > 0) { // time > appUserTime
        QX11Info::setAppUserTime(time);
    }
    if (QX11Info::appTime() == 0
            || NET::timestampCompare(time, QX11Info::appTime()) > 0) { // time > appTime
        QX11Info::setAppTime(time);
    }
#else
    Q_UNUSED(time)
#endif
}

